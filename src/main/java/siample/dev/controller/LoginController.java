package siample.dev.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import siample.dev.domain.User;
import siample.dev.form.LoginForm;
import siample.dev.service.UserService;


@Controller
public class LoginController {
	
	
	@Autowired(required=true)
	UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginForm() {
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@ModelAttribute(value = "anything") LoginForm loginForm, Model model) {

		System.out.println("POST Http form request submitted.");

		LoginForm model_lf = (LoginForm) model.getAttribute("anything");
		String model_un = model_lf.getUsername();
		String model_pw = model_lf.getPassword();

		System.out.println("submitted in Model model: " + model_un + " : " + model_pw);
		System.out.println("submitted into loginForm: " + loginForm.getUsername() + " : " + loginForm.getPassword());
		List<User> h2_users = userService.getAllUsers(); System.out.println("H2 users: "); System.out.println(h2_users.toString());
	 

		for (User h2_user : h2_users) {

		//	System.out.println("H2 users: " + user.getUsername() + " : " + user.getPassword());

			if (h2_user.getUsername().equals(model_un) && (h2_user.getPassword().equals(model_pw))) {
				return "home";
			}
		}
		
		model.addAttribute("invalidCredentials", true);
		return "login";
	}
}
