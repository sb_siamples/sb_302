package siample.dev.repository;

import org.springframework.data.repository.CrudRepository;
import siample.dev.domain.User;
import java.util.List;

public interface UserRepo extends CrudRepository<User, Long>{

	public List<User> findAll();
	
}
