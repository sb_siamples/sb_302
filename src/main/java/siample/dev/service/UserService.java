package siample.dev.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import siample.dev.domain.User;
import siample.dev.repository.UserRepo; 

@Service
public class UserService {
	
	@Autowired
	UserRepo userRepo;
	
	public List<User> getAllUsers(){
		return userRepo.findAll();
	}

}
